package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.dragosh.tm.entity.Project;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProjectRepositoryImplement implements ProjectRepository {
    @NotNull
    private final Map<String, Project> projects = new HashMap<>();

    @Override
    public List<Project> getAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public Project getById(@NotNull final String id) {
        return projects.get(id);
    }

    @Override
    public Project getByName(String name) {
        return projects.values().stream()
                .filter(project -> project.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void persist(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void merge(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void delete(@NotNull final String id) {
        projects.remove(id);
    }
}
