package ru.dragosh.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.dragosh.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class TaskRepositoryImplement implements TaskRepository {
    @NotNull
    private final Map<String, Task> tasks = new HashMap<>();

    @Override
    public List<Task> getAll() {
        return new ArrayList<>(tasks.values());
    }

    @Override
    public List<Task> getAll(@NotNull final String projectId) {
        return tasks.values().stream().filter(task -> task.getProjectId().equals(projectId)).collect(Collectors.toList());
    }

    @Override
    public Task getById(@NotNull final String projectId, @NotNull final String taskId) {
        return tasks.values().stream()
                .filter(task -> task.getProjectId().equals(projectId) && task.getId().equals(taskId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void persist(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void merge(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void deleteByTaskId(String id) {
        tasks.remove(id);
    }

    @Override
    public void deleteByProjectId(String id) {
        List<Task> taskList = new ArrayList<>(tasks.values());
        taskList.stream()
                .filter(task -> task.getProjectId().equals(id))
                .forEach(task -> tasks.remove(task.getId()));
    }

    @Override
    public void deleteByProjectIdAndTaskId(String projectId, String taskId) {
        List<Task> taskList = new ArrayList<>(tasks.values());
        taskList.stream()
                .filter(task -> task.getProjectId().equals(projectId) && task.getId().equals(taskId))
                .forEach(task -> tasks.remove(task.getId()));
    }

    @Override
    public Task getByProjectIdAndName(String projectId, String name) {
        return tasks.values().stream()
                .filter(task -> task.getProjectId().equals(projectId) && task.getName().equals(name)).findFirst().orElse(null);
    }
}
