package ru.dragosh.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.dragosh.tm.enumarate.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Task {
    private String id = UUID.randomUUID().toString();
    private String name;
    private String description;
    private Date dateStart;
    private Date dateFinish;
    private String projectId;
    private Status status;

    public Task (
            String name,
            String description,
            Date dateStart,
            Date dateFinish,
            String projectId,
            Status status
    ) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
        this.projectId = projectId;
        this.status = status;
    }
}
