package ru.dragosh.tm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.dragosh.tm.repository.ProjectRepository;
import ru.dragosh.tm.repository.ProjectRepositoryImplement;
import ru.dragosh.tm.repository.TaskRepository;
import ru.dragosh.tm.repository.TaskRepositoryImplement;
import ru.dragosh.tm.service.ProjectService;
import ru.dragosh.tm.service.ProjectServiceImplement;
import ru.dragosh.tm.service.TaskService;
import ru.dragosh.tm.service.TaskServiceImplement;

@EnableWebMvc
@ComponentScan("ru.dragosh.tm")
@Configuration
public class SpringConfig implements WebMvcConfigurer {
    @Bean
    public InternalResourceViewResolver resolver() {
        final InternalResourceViewResolver resolver =
                new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Bean
    public ProjectService getProjectService() {
        return new ProjectServiceImplement();
    }

    @Bean
    public TaskService getTaskService() {
        return new TaskServiceImplement();
    }

    @Bean
    public ProjectRepository getProjectRepository() {
        return new ProjectRepositoryImplement();
    }

    @Bean
    public TaskRepository getTaskRepository() {
        return new TaskRepositoryImplement();
    }
}
